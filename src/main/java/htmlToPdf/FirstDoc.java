package htmlToPdf;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import org.xhtmlrenderer.pdf.ITextRenderer;

import com.lowagie.text.DocumentException;



public class FirstDoc {

	private static float dotsPerPoint = 20f * 4f / 3f;
	private static int dotsPerPixel = 50;
	
    public static void main(String[] args) 
            throws IOException, DocumentException {
        String inputFile = "samples/firstdoc.xhtml";
        String url = new File(inputFile).toURI().toURL().toString();
        String outputFile = "firstdoc.pdf";
        OutputStream os = new FileOutputStream(outputFile);
        
        ITextRenderer renderer = new ITextRenderer(dotsPerPoint, dotsPerPixel);
        
        renderer.setDocument(url);
        renderer.layout();
        renderer.createPDF(os);

        System.out.println("finished");
        
        os.close();
    }
}
