package htmlToPdf;

import com.lowagie.text.DocumentException;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import org.xhtmlrenderer.pdf.ITextRenderer;

public class DirectXML {
    public static void main(String[] args) throws IOException, DocumentException {
        String inputFile = "samples/weather.xml";
        String outputFile = "weather.pdf";
        
        OutputStream os = new FileOutputStream(outputFile);
        ITextRenderer renderer = new ITextRenderer();
        renderer.setDocument(new File(inputFile));
        renderer.layout();
        renderer.createPDF(os);
        os.close();
    }
}
